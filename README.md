# `cvmake`

## Similar existing tools

* [JSON Resume](https://jsonresume.org/) ([on GitHub](https://github.com/jsonresume))
* [Resume Fodder](https://resumefodder.com/) for converting the above output to Word document format

## Design considerations

* Be able to order things by actual dates/ranges
* Be able to class Big Things as past or current
* Be able to class things as e.g. education or work experience, etc
* Be able to both associate bullet points with their heading entry, but also classify/tag them so you can select subsets and order them by those tags/classifications
* Be able to give preferences in order, e.g. do current then past, within each of those do education first and then work experience, give bullet points in set tag order, and limit to two A4 pages, so omit bullet points of lowest priority tags (first of past work experience, then of past education, then current work experience, then current education) until 2 pages limit is satisfied --- of course assumption here being that there's too much content rather than too little
